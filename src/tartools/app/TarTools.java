package tartools.app;

import tartools.archive.Archive;
import tartools.archive.factory.ArchiveFactory;
import tartools.archive.factory.TarArchiveFactory;
import tartools.archive.options.OptionsManager;
import tartools.archive.options.filters.ArchiveFileFilter;
import tartools.archive.options.filters.FilterFactory;
import tartools.archive.visitor.ArchiveVisitor;
import tartools.archive.visitor.ArchiveVisitorFactory;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe principale de l'application TarTools
 * Utilisation en mode console
 */
public class TarTools {

    private static final Scanner INPUT = new Scanner(System.in);

    /**
     * Méthode de lancement de l'application
     *  - Condition d'arrêt : entrée de "exit"
     * @param args non utilisé
     */
    public static void main(String[] args) {
        //Si l'utilisateur à fourni des paramètres dans les arguments de lancement
        if(args.length > 0) {
            //On récupère la commande si elle est connue
            ArchiveVisitor command = parseCommandLine(args);
            if (command != null) {
                //On execute la commande
                executeCommand(command, Arrays.copyOfRange(args, 1, args.length));
            }
            return;
        }

        //Demande l'entrée à l'utilisateur
        String commandLine = getCommandLine();
        //Boucle tant que l'entrée est différente de "exit"
        while(!commandLine.equals("exit")) {
            if(commandLine.length() > 0) {
                //On découpe l'entrée en faisant attention aux guillemets
                String[] commandTab = commandLineToArray(commandLine);

                //On récupère la commande si elle est connue
                ArchiveVisitor command = parseCommandLine(commandTab);
                if (command != null) {
                    //On execute la commande
                    executeCommand(command, Arrays.copyOfRange(commandTab, 1, commandTab.length));
                }
            }

            //Demande l'entrée à l'utilisateur
            commandLine = getCommandLine();
        }
    }

    /**
     * Permet de transformer la ligne de commande en tableau de chaîne de caractère
     * @param commandLine la ligne de commande
     * @return le tableau représentant la ligne de commande
     */
    private static String[] commandLineToArray(String commandLine) {
        //On découpe l'entrée en faisant attention aux guillemets
        List<String> list = new ArrayList<String>();
        Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(commandLine);
        while (m.find()) {
            list.add(m.group(1).replace("\"", ""));
        }
        String[] commandTab = new String[list.size()];
        return list.toArray(commandTab);
    }

    /**
     * Demande une ligne de commande à l'utilisateur
     * @return la ligne de commande
     */
    private static String getCommandLine() {
        System.out.print("TarTools> ");
        return INPUT.nextLine();
    }

    /**
     * Récupère la commande correspondant à l'entrée utilisateur
     * @param commandTab entrée utilisateur sous forme de tableau
     * @return la commande ou null si la commande n'a pas pu être trouvée
     */
    private static ArchiveVisitor parseCommandLine(String[] commandTab) {
        try {
            return ArchiveVisitorFactory.createVisitor(commandTab[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            System.out.println("Commande "+commandTab[0]+" non reconnue.");
            return null;
        }
    }

    /**
     * Execute la commande avec les paramètres fournis
     * @param command Commande à executer
     * @param commandTab entrée utilisateur privée du nom de la commande (paramètres)
     */
    private static void executeCommand(ArchiveVisitor command, String[] commandTab) {
        try {
            //On récupère les options
            OptionsManager options = parseOptions(commandTab);
            if(!options.containsKey("$1")) {
                System.out.println("Cette commande necessite une archive en paramètre.");
                return;
            }

            //On vérifie l'extension du fichier passé en paramètre
            String extension = options.get("$1").substring(options.get("$1").indexOf('.')+1).toLowerCase();
            ArchiveFactory factory;
            switch (extension) {
                case "tar" :
                    factory = new TarArchiveFactory();
                    break;
                default:
                    System.out.println("L'extention "+extension+" n'est pas prise en charge.");
                    return;
            }

            //On construit l'objet archive
            Archive archive = factory.createArchive(options.get("$1"));

            //On récupère puis configure le visiteur qui permettra d'executer la commande
            command.setOptionsManager(options);

            //On effectue la commande
            archive.accept(command);

        }catch (IOException e) {
            System.out.println("Le fichier n'a pas pu être lu.");
        }
    }

    private static OptionsManager parseOptions(String[] opt) {
        //On construit la liste des options
        Map<String, String> options = new HashMap<String, String>();
        String optionBefore = null;
        int optCount = 1;
        for (String entry : opt) {
            if(entry.startsWith("-")) {
                String key = entry.substring(entry.indexOf('-')+1);
                options.put(key,null);
                optionBefore = key;
            } else if(optionBefore != null) {
                options.replace(optionBefore,entry);
                optionBefore = null;
            } else {
                options.put("$"+optCount,entry);
                optCount++;
            }
        }
        //On construit la liste des filtres à partir de ces options
        List<ArchiveFileFilter> filterList = new ArrayList<ArchiveFileFilter>();
        FilterFactory filterFactory = new FilterFactory();
        for(Map.Entry entry : options.entrySet()) {
            try {
                if( entry.getValue() != null) {
                    ArchiveFileFilter filter = FilterFactory.createFilter( (String)entry.getKey(), (String)entry.getValue() );
                    filter.setValue((String)entry.getValue());
                    filterList.add(filter);
                }
            } catch (IllegalArgumentException e) {}
        }

        return new OptionsManager(filterList,options);
    }
}
