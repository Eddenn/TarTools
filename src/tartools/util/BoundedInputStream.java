package tartools.util;

import java.io.IOException;
import java.io.InputStream;

public class BoundedInputStream extends InputStream {
    private final InputStream inputStream;
    private long length;

    public BoundedInputStream(InputStream inputStream, long length) {
        this.inputStream = inputStream;
        this.length = length;
    }

    @Override
    public int read() throws IOException {
        return (length-- <= 0) ? -1 : inputStream.read();
    }
}
