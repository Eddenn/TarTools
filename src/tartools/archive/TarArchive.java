package tartools.archive;

import tartools.archive.file.ArchiveFile;
import tartools.archive.visitor.ArchiveVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe représentant une archive tar
 */
public class TarArchive implements Archive{

    private List<ArchiveFile> files;

    /**
     * Constructeur
     */
    public TarArchive() {
        files = new ArrayList<ArchiveFile>();
    }

    @Override
    public void accept(ArchiveVisitor v) {
        v.visitTarArchive(this);
    }

    @Override
    public List<ArchiveFile> getFiles() {
        return files;
    }

    @Override
    public void addFile(ArchiveFile file) {
        files.add(file);
    }

    @Override
    public void removeFile(int i) {
        files.remove(i);
    }

    @Override
    public void removeFile(String fileName) {
        ArchiveFile aToRemove = null;
        for(ArchiveFile a : files) {
            if(a.getHeader().getName().trim().equals(fileName)) {
                aToRemove = a;
                break;
            }
        }
        if(aToRemove != null) {
            files.remove(aToRemove);
        }
    }

    @Override
    public ArchiveFile getFile(int i) {
        return files.get(i);
    }

    @Override
    public ArchiveFile getFile(String fileName) {
        for(ArchiveFile a : files) {
            if(a.getHeader().getName().trim().equals(fileName)) {
                return a;
            }
        }
        return null;
    }
}
