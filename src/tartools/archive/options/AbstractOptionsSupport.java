package tartools.archive.options;

public abstract class AbstractOptionsSupport {

    private OptionsManager optionsManager;

    public AbstractOptionsSupport() {
        this.optionsManager = new OptionsManager();
    }

    public AbstractOptionsSupport(OptionsManager optionsManager) {
        this.optionsManager = optionsManager;
    }

    public OptionsManager getOptionsManager() {
        return optionsManager;
    }

    public void setOptionsManager(OptionsManager optionsManager) {
        this.optionsManager = optionsManager;
    }
}
