package tartools.archive.options;

import tartools.archive.file.ArchiveFile;
import tartools.archive.options.filters.ArchiveFileFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptionsManager {

    private Map<String,String> options;
    private List<ArchiveFileFilter> filterList;

    public OptionsManager() {
        this( new ArrayList<>() , new HashMap<>() );
    }

    public OptionsManager(Map<String,String> options) {
        this( new ArrayList<>() , options );
    }

    public OptionsManager(List<ArchiveFileFilter> filterList) {
        this( filterList , new HashMap<>() );
    }

    public OptionsManager(List<ArchiveFileFilter> filterList, Map<String,String> options) {
        this.options = options;
        this.filterList = filterList;
    }

    public String get(String key) {
        return options.get(key);
    }

    public boolean containsKey(String key) {
        return options.containsKey(key);
    }

    public Map<String,String> getOptions() {
        return options;
    }

    public void setOptions(Map<String,String> options) {
        this.options = options;
    }

    public List<ArchiveFileFilter> getFilterList() {
        return filterList;
    }

    public void setFilterList(List<ArchiveFileFilter> filterList) {
        this.filterList = filterList;
    }

    public boolean validateAllFilter(ArchiveFile archiveFile) {
        for(ArchiveFileFilter filter : filterList) {
            if(!filter.validateFilter(archiveFile)) {
                return false;
            }
        }
        return true;
    }
}
