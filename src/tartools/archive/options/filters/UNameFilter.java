package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;
import tartools.archive.file.header.ExtendedTarHeader;

public class UNameFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public UNameFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        if(archiveFile.getHeader().getMagic().equals("ustar")) {
            return ((ExtendedTarHeader)archiveFile.getHeader()).getUName().equals( getValue() );
        }
        return false;
    }
}
