package tartools.archive.options.filters;

public abstract class AbstractArchiveFileFilter {

    private String value = null;

    public AbstractArchiveFileFilter(String value) {
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
