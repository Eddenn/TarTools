package tartools.archive.options.filters;

public class FilterFactory {

    public static ArchiveFileFilter createFilter(String key, String value) throws IllegalArgumentException{
        switch (key) {
            case "n" : return new PathNameFilter(value);
            case "nx" : return new RegexFilter(value);
            case "u" : return new UidFilter(value);
            case "g" : return new GidFilter(value);
            case "ui" : return new UNameFilter(value);
            case "gi" : return new GNameFilter(value);
            case "sl" : return new MaxSizeFilter(value);
            case "sg" : return new MinSizeFilter(value);
        }
        throw new IllegalArgumentException("Le filtre dont la clé est ("+key+") n'est pas supporté.");
    }

}
