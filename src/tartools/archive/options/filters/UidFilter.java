package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

public class UidFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public UidFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        return archiveFile.getHeader().getUid().equals( getValue() );
    }
}
