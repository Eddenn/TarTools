package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

public class GidFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public GidFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        return archiveFile.getHeader().getGid().equals( getValue() );
    }
}
