package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

public interface ArchiveFileFilter {

    /**
     * Fonction permettant de vérifier si un fichier d'archive valide la condition du filtre
     * @param archiveFile fichier d'archive
     * @return vrai si le fichier d'archive valide le filtre, faux sinon
     */
    boolean validateFilter(ArchiveFile archiveFile);

    /**
     * Permet la modification de la valeur liée au filtre
     * @param value valeur
     */
    public void setValue(String value);

    /**
     * Accesseur de la valeur liée au filtre
     * @return valeur
     */
    public String getValue();
}
