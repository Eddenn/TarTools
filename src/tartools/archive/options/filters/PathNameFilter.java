package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

public class PathNameFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public PathNameFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        return archiveFile.getHeader().getFullName().equals( getValue() );
    }
}
