package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

public class MinSizeFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public MinSizeFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        try {
            if (archiveFile.getHeader().getSize() > Integer.parseInt(getValue())) {
                return true;
            }
        }catch (NumberFormatException e) {
            return false;
        }
        return false;
    }
}
