package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter{

    public RegexFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        // compilation de la regex
        Pattern p = Pattern.compile(getValue());
        // création d'un moteur de recherche
        Matcher m = p.matcher(archiveFile.getHeader().getFullName());
        // lancement de la recherche de toutes les occurrences
        return m.matches();
    }
}
