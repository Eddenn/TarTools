package tartools.archive.options.filters;

import tartools.archive.file.ArchiveFile;
import tartools.archive.file.header.ExtendedTarHeader;

public class GNameFilter extends AbstractArchiveFileFilter implements ArchiveFileFilter {

    public GNameFilter(String value) {
        super(value);
    }

    @Override
    public boolean validateFilter(ArchiveFile archiveFile) {
        if(archiveFile.getHeader().getMagic().equals("ustar")) {
            return ((ExtendedTarHeader)archiveFile.getHeader()).getGName().equals( getValue() );
        }
        return false;
    }
}
