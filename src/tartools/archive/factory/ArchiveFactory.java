package tartools.archive.factory;

import tartools.archive.Archive;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.MalformedInputException;

public interface ArchiveFactory {

    public Archive createArchive(String pathToArchive) throws IOException;
}
