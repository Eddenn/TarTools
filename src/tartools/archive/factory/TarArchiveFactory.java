package tartools.archive.factory;

import tartools.archive.Archive;
import tartools.archive.TarArchive;
import tartools.archive.file.*;
import tartools.archive.file.header.ArchiveFileHeader;
import tartools.archive.file.header.BasicTarHeader;
import tartools.archive.file.header.ExtendedTarHeader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Factory permettant de constuire une TarArchive à partir d'un chemin d'accès à une archive
 */
public class TarArchiveFactory implements ArchiveFactory {

    @Override
    public Archive createArchive(String archiveName) throws IOException {
        Archive archive = new TarArchive();

        //Lecture du fichier
        InputStream inputstream = new FileInputStream(archiveName);

        //Initialisation de variable utiles
        byte[] buffer = new byte[512];
        boolean isHeader = true;
        ArchiveFileHeader archiveFileHeader = null;
        ArchiveFile archiveFile = null;
        int fileSize;
        int indexInArchive = 0;
        List<DirectoryArchiveFile> directoryArchiveFileList = new ArrayList<DirectoryArchiveFile>();
        List<ArchiveFile> otherArchiveFileList = new ArrayList<ArchiveFile>();

        //Lis le fichier par paquets de 512 octets
        while( inputstream.available() > 0 ) {
            if(isHeader) {
                //On récupère l'en-tête
                if( inputstream.read(buffer) != 512 ) {
                    System.out.println("## Fail - Bad header size ##");
                    break;
                }
                indexInArchive = indexInArchive + 512;
                if( buffer[0] == 0) {
                    break;
                }

                //On crée l'entête
                archiveFileHeader = buildHeader(buffer);

                isHeader = false;
            } else {
                //On passe le contenu du fichier
                fileSize = archiveFileHeader.getSize();
                int blockToSkip;
                if(fileSize == 0) {
                    blockToSkip = 0;
                } else {
                    blockToSkip = ((fileSize/512)+1);
                }
                if(inputstream.skip( blockToSkip*512 ) != blockToSkip*512) {
                    System.out.println("## Fail - Bad content size ##");
                    break;
                }

                indexInArchive = indexInArchive + blockToSkip*512;

                //On crée le fichier
                int typeflag = archiveFileHeader.getTypeFlag();
                boolean typeFlagSupported = true;
                switch (archiveFileHeader.getTypeFlag()) {
                    case 5 ://Ajout du répertoire dans la liste des répertoires
                            DirectoryArchiveFile dir = new DirectoryArchiveFile(archiveFileHeader, archiveName, indexInArchive);
                            directoryArchiveFileList.add(dir);

                            archiveFile = dir;
                            break;
                    case 0 ://Ajout du fichier dans la liste des fichiers
                            ArchiveFile file = new StandardArchiveFile(archiveFileHeader, archiveName, indexInArchive);
                            otherArchiveFileList.add(file);

                            archiveFile = file;
                            break;
                    case 1 ://Ajout du lien matériel dans la liste des fichiers
                            ArchiveFile hardLink = new HardLinkArchiveFile(archiveFileHeader, archiveName, indexInArchive);
                            otherArchiveFileList.add(hardLink);

                            archiveFile = hardLink;
                            break;
                    case 2 ://Ajout du lien symbolique dans la liste des fichiers
                            ArchiveFile symbolicLink = new SymbolicLinkArchiveFile(archiveFileHeader, archiveName, indexInArchive);
                            otherArchiveFileList.add(symbolicLink);

                            archiveFile = symbolicLink;
                            break;
                    default: typeFlagSupported = false;
                }
                if(typeFlagSupported) {
                    //On l'ajoute à l'objet d'archive s'il est à la racine
                    String archiveFileName = archiveFileHeader.getName().trim();
                    int countSlash = archiveFileName.length() - archiveFileName.replace("/", "").length();
                    //  (Si il contient un slash ou moins)
                    if(typeflag == 5 && countSlash <= 1)  archive.addFile(archiveFile);
                    if(typeflag != 5 && countSlash == 0)  archive.addFile(archiveFile);
                } else {
                    System.out.println("Le type de fichier "+archiveFileHeader.getTypeFlag()+" n'est pas pris en charge par l'application.");
                }

                isHeader = true;
            }
        }

        //Lien des fichiers sur les répertoires
        for(DirectoryArchiveFile dir : directoryArchiveFileList) {
            String dirName = dir.getHeader().getName().trim();
            for(ArchiveFile file : otherArchiveFileList) {
                String fileName = file.getHeader().getName().trim();
                String fileDirName = fileName.substring(0,fileName.lastIndexOf('/')+1);
                if( fileDirName.equals(dirName) ) {
                    dir.addChild(file);
                }
            }
        }

        //Lien des sous-répertoires sur leurs parents
        for(DirectoryArchiveFile dir : directoryArchiveFileList) {
            String dirName = dir.getHeader().getName().trim();
            for(DirectoryArchiveFile subdir : directoryArchiveFileList) {
                String subDirName = subdir.getHeader().getName().trim();
                //On utilise les nom de fichier pour créer la hiérarchie
                if(subDirName.lastIndexOf('/') != -1) {
                    String subDirCompareName = subDirName.substring(0,subDirName.lastIndexOf('/'));
                    if(subDirCompareName.lastIndexOf('/') != -1) {
                        subDirCompareName = subDirCompareName.substring(0,subDirCompareName.lastIndexOf('/')+1);
                        if(subDirCompareName.equals(dirName)) {
                            dir.addChild(subdir);
                        }
                    }
                }
            }
        }

        return archive;
    }

    /**
     * Construit un ArchiveFileHeader en fonction du buffer
     * @param buffer entête sous forme de tableau d'octets
     * @return entête sous forme d'ArchiveFileHeader
     */
    private ArchiveFileHeader buildHeader(byte[] buffer) {
        if(buffer == null || buffer.length != 512) {
            throw new IllegalArgumentException("Le buffer doit contenir au moins 512 octets.");
        }
        Map<String,byte[]> headerMap = new HashMap<String, byte[]>();

        int index=0;
        headerMap.put("name",Arrays.copyOfRange(buffer,0,100));
        headerMap.put("mode",Arrays.copyOfRange(buffer,100,108));
        headerMap.put("uid",Arrays.copyOfRange(buffer,108,116));
        headerMap.put("gid",Arrays.copyOfRange(buffer,116,124));
        headerMap.put("size",Arrays.copyOfRange(buffer,124,136));
        headerMap.put("mtime",Arrays.copyOfRange(buffer,136,148));
        headerMap.put("chksum",Arrays.copyOfRange(buffer,148,156));
        headerMap.put("typeflag",Arrays.copyOfRange(buffer,156,157));
        headerMap.put("linkname",Arrays.copyOfRange(buffer,157,257));

        //Si le champ de l'en-tête "magic" vaut "ustar" alors l'en-tête est étendu
        headerMap.put("magic",Arrays.copyOfRange(buffer,257,263));
        boolean isExtendedHeader;
        try {
            String magic = new String(headerMap.get("magic"), "US-ASCII");
            isExtendedHeader = magic.trim().equals("ustar");
        }catch (UnsupportedEncodingException e) {
            isExtendedHeader = false;
        }

        ArchiveFileHeader archiveFileHeader;
        //Donc si il est étendu on recherche ses champs supplémentaires
        if(isExtendedHeader) {
            headerMap.put("version", Arrays.copyOfRange(buffer, 263, 265));
            headerMap.put("uname", Arrays.copyOfRange(buffer, 265, 297));
            headerMap.put("gname", Arrays.copyOfRange(buffer, 297, 329));
            headerMap.put("devmajor", Arrays.copyOfRange(buffer, 329, 337));
            headerMap.put("devminor", Arrays.copyOfRange(buffer, 337, 345));
            headerMap.put("prefix", Arrays.copyOfRange(buffer, 345, 500));
            archiveFileHeader = new ExtendedTarHeader(headerMap);
        } else {
            archiveFileHeader = new BasicTarHeader(headerMap);
        }

        return archiveFileHeader;
    }
}
