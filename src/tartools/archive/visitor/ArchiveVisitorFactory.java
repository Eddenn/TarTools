package tartools.archive.visitor;

public class ArchiveVisitorFactory {

    public static ArchiveVisitor createVisitor(String command) throws IllegalArgumentException{
        switch (command.toUpperCase()) {
            case "LIST" : return new ListArchiveVisitor();
            case "EXTRACT" : return new ExtractArchiveVisitor();
        }
        throw new IllegalArgumentException("La commande "+command+" n'est pas supportée.");
    }

}
