package tartools.archive.visitor;

import tartools.archive.TarArchive;
import tartools.archive.options.OptionsManager;

public interface ArchiveVisitor {

    /**
     * Retourne le gestionnaire d'options
     * @return gestionnaire d'options
     */
    OptionsManager getOptionsManager();

    /**
     * Modifie le gestionnaire d'options
     * @param optionsManager gestionnaire d'options
     */
    void setOptionsManager(OptionsManager optionsManager);

    /**
     * Permet de visiter une TarArchive
     * @param tarArchive archive à visiter
     */
    void visitTarArchive(TarArchive tarArchive);
}
