package tartools.archive.visitor;

import tartools.archive.TarArchive;
import tartools.archive.file.ArchiveFile;
import tartools.archive.file.visitor.ListArchiveFileVisitor;
import tartools.archive.options.AbstractOptionsSupport;
import tartools.archive.options.OptionsManager;

import java.util.Map;

public class ListArchiveVisitor extends AbstractOptionsSupport implements ArchiveVisitor {

    public ListArchiveVisitor() {
        super();
    }

    public ListArchiveVisitor(OptionsManager options) {
        super(options);
    }

    /**
     * Execute la commande de listage de l'archive tar
     * @param tarArchive archive à visiter
     */
    @Override
    public void visitTarArchive(TarArchive tarArchive) {
        if(tarArchive == null) {
            System.out.println("USAGE : list archive.tar options");
            return;
        }
        for(ArchiveFile a : tarArchive.getFiles()) {
            a.accept( new ListArchiveFileVisitor(getOptionsManager()) );
        }
    }
}
