package tartools.archive.visitor;

import tartools.archive.TarArchive;
import tartools.archive.file.ArchiveFile;
import tartools.archive.file.DirectoryArchiveFile;
import tartools.archive.file.visitor.ExtractArchiveFileVisitor;
import tartools.archive.options.AbstractOptionsSupport;
import tartools.archive.options.OptionsManager;
import tartools.util.BoundedInputStream;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ExtractArchiveVisitor extends AbstractOptionsSupport implements ArchiveVisitor {

    public ExtractArchiveVisitor() {
        super();
    }

    public ExtractArchiveVisitor(OptionsManager optionsManager) {
        super(optionsManager);
    }

    /**
     * Execute la commande d'extraction de l'archive tar
     * @param tarArchive archive à visiter
     */
    @Override
    public void visitTarArchive(TarArchive tarArchive) {
        //Vérifie que la destination est bien fournie
        if(!getOptionsManager().getOptions().containsKey("$2")) {
            System.out.println("Cette commande necessite une destination.");
            System.out.println("USAGE : extract archive.tar cible options");
            return;
        }
        //Vérifie que le répertoire de destination existe
        String dest = getOptionsManager().getOptions().get("$2");
        File f = new File(dest);
        if(!f.exists()) {
            System.out.println("Le répertoire de destination n'existe pas.");
            return;
        }
        if(!f.isDirectory()) {
            System.out.println("Le répertoire de destination n'est pas un répertoire.");
            return;
        }
        if(f.listFiles() != null) {
            for (File file : f.listFiles()) {
                deleteRecur(file);
            }
        }

        //On écrit les fichiers
        for(ArchiveFile a :tarArchive.getFiles()) {
            a.accept( new ExtractArchiveFileVisitor(dest, getOptionsManager()) );
        }

    }

    /**
     * Permet de supprimer récursivement le contenu d'un répertoire
     * @param dir répertoire
     */
    private void deleteRecur(File dir) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory()) deleteRecur(file);
            file.delete();
        }
    }

    /**
     * Extrait la liste des fichiers (archiveFileList) à la destination (dest)
     * @param dest répertoire de destination
     * @param archiveFileList liste des fichiers à extraire
     * @throws IOException
     */
    private void extractFiles(String dest, List<ArchiveFile> archiveFileList) throws IOException {
        for(ArchiveFile a :archiveFileList) {
            if(a instanceof DirectoryArchiveFile) {
                extractFiles(dest, ((DirectoryArchiveFile)a).getChilds());
            } else {
                //On récupère le nom relatif à l'archive
                String fileName = a.getHeader().getFullName();
                //On crée le chemin total
                Path pathToFile = Paths.get(dest + "/" + fileName);
                //On crée les répertoires manquants
                Files.createDirectories(pathToFile.getParent());
                //On crée le fichier
                Files.createFile(pathToFile);
                //----- On écrit le fichier en fonction de son contenu -----
                File fileCreated = pathToFile.toFile();
                //On estime son Charset
                // (Arbitrairement UTF-8 car necessiterai une détection par une librairie externe ou autre algorithme lourd)
                Charset charset = StandardCharsets.UTF_8;
                //On crée le writer
                OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(fileCreated), charset);
                //On récupère le contenu du fichier
                BoundedInputStream inputStream = a.getContent();
                BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, charset));
                //On écrit dans le fichier
                int cReaded;
                while ((cReaded = in.read()) != -1) {
                    fileWriter.write(cReaded);
                }
                //On ferme les stream
                in.close();
                inputStream.close();
                fileWriter.close();
                //----------------------------------------------------------
            }
        }
    }
}
