package tartools.archive.file;

import tartools.archive.file.header.ArchiveFileHeader;
import tartools.archive.file.header.ExtendedTarHeader;
import tartools.util.BoundedInputStream;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe représentant un fichier de l'archive
 * Patron : Composite
 */
public abstract class ArchiveFile implements VisitableArchiveFile {
    private ArchiveFileHeader header;
    private String archiveFileName;
    private int index;

    /**
     * Constructeur
     * @param header en-tête du fichier
     * @param archiveFileName nom du fichier de l'archive
     * @param index emplaçement dans l'archive
     */
    public ArchiveFile(ArchiveFileHeader header, String archiveFileName, int index) {
        this.header = header;
        this.archiveFileName = archiveFileName;
        this.index = index;
    }

    /**
     * Accesseur de l'en-tête
     * @return l'en-tête du fichier
     */
    public ArchiveFileHeader getHeader() {
        return header;
    }

    /**
     * Renvoie le contenu du fichier sous forme de BoundedInputStream placé au début du contenu de celui-ci
     * Attention : L'InputStream étant positionné sur l'archive, il vous faut vérifier le nombre d'octets que vous lisez (getHeader().getSize())
     * @return le contenu du fichier
     */
    public BoundedInputStream getContent() throws IOException {
        InputStream inputstream = new FileInputStream(archiveFileName);
        BoundedInputStream boundedInputStream = new BoundedInputStream(inputstream,header.getSize());
        long n;
        if((n = inputstream.skip(index)) != index) {
            throw new IllegalStateException("Déplaçement jusqu'au contenu du fichier - la taille du fichier n'est pas correct. ("+n+"octets au lieu de "+index+" octets)");
        }
        return boundedInputStream;
    }

    /**
     * Retourne une chaîne de caractère décrivant le fichier (nom du fichier)
     * @return la chaîne de caractère décrivant le fichier (nom du fichier)
     */
    public String toString() {
        return header.getFullName();
    }
}
