package tartools.archive.file;

import tartools.archive.file.visitor.ArchiveFileVisitor;

/**
 * Interface permettant de rendre visitable un fichier d'archive
 */
public interface VisitableArchiveFile {

    /**
     * Méthode permettant l'exécution d'une commande sur un fichier d'archive
     * @param v le visiteur executant la commande sur un fichier d'archive
     */
    void accept(ArchiveFileVisitor v);
}
