package tartools.archive.file;

import tartools.archive.file.header.ArchiveFileHeader;
import tartools.archive.file.visitor.ArchiveFileVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un répertoire de l'archive
 * Patron : Composite
 */
public class DirectoryArchiveFile extends ArchiveFile {

    private List<ArchiveFile> components;

    /**
     * Constructeur
     * @param header en-tête du fichier
     * @param archiveFileName nom du fichier de l'archive
     * @param index emplaçement dans l'archive
     */
    public DirectoryArchiveFile(ArchiveFileHeader header, String archiveFileName, int index) {
        super(header, archiveFileName, index);
        components = new ArrayList<ArchiveFile>();
    }

    /**
     * Méthode permettant l'ajout d'une sous entrée de fichier
     * @param archiveFile le fichier à ajouter
     */
    public void addChild(ArchiveFile archiveFile) {
        components.add(archiveFile);
    }

    /**
     * Méthode permettant la suppression d'une sous entrée de fichier
     * @param archiveFile le fichier à supprimer
     */
    public void removeChild(ArchiveFile archiveFile) {
        components.remove(archiveFile);
    }

    /**
     * Méthode permettant la récupération d'une sous entrée de fichier
     * @param i l'index dans la liste, du fichier à récupérer
     */
    public ArchiveFile getChild(int i) {
        return components.get(i);
    }

    /**
     * Accesseur des sous fichiers
     * @return liste des sous fichiers
     */
    public List<ArchiveFile> getChilds() {
        return components;
    }

    @Override
    public String toString() {
        return '['+super.toString()+']';
    }

    @Override
    public void accept(ArchiveFileVisitor v) {
        v.visitDirectoryArchiveFile(this);
        for(ArchiveFile a : components) {
            a.accept(v);
        }
    }
}
