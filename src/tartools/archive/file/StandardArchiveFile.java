package tartools.archive.file;

import tartools.archive.file.header.ArchiveFileHeader;
import tartools.archive.file.visitor.ArchiveFileVisitor;

/**
 * Classe représentant un fichier simple de l'archive
 * Patron : Composite
 */
public class StandardArchiveFile extends ArchiveFile {

    /**
     * Constructeur
     * @param header en-tête du fichier
     * @param archiveFileName nom du fichier de l'archive
     * @param index emplaçement dans l'archive
     */
    public StandardArchiveFile(ArchiveFileHeader header, String archiveFileName, int index) {
        super(header, archiveFileName, index);
    }

    @Override
    public void accept(ArchiveFileVisitor v) {
        v.visitStandardArchiveFile(this);
    }
}
