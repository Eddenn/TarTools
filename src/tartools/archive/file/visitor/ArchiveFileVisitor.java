package tartools.archive.file.visitor;

import tartools.archive.file.HardLinkArchiveFile;
import tartools.archive.file.StandardArchiveFile;
import tartools.archive.file.DirectoryArchiveFile;
import tartools.archive.file.SymbolicLinkArchiveFile;
import tartools.archive.options.OptionsManager;

public interface ArchiveFileVisitor {

    /**
     * Retourne le gestionnaire d'options
     * @return gestionnaire d'options
     */
    OptionsManager getOptionsManager();

    /**
     * Modifie le gestionnaire d'options
     * @param optionsManager gestionnaire d'options
     */
    void setOptionsManager(OptionsManager optionsManager);

    /**
     * Permet de visiter un répertoire d'archive
     * @param directoryArchiveFile répertoire d'archive à visiter
     */
    void visitDirectoryArchiveFile(DirectoryArchiveFile directoryArchiveFile);

    /**
     * Permet de visiter un fichier d'archive
     * @param standardArchiveFile fichier d'archive à visiter
     */
    void visitStandardArchiveFile(StandardArchiveFile standardArchiveFile);

    /**
     * Permet de visiter un fichier d'archive
     * @param symbolicLinkArchiveFile le lien symbolique d'archive à visiter
     */
    void visitSymbolicLinkArchiveFile(SymbolicLinkArchiveFile symbolicLinkArchiveFile);

    /**
     * Permet de visiter un fichier d'archive
     * @param hardLinkArchiveFile le lien matériel d'archive à visiter
     */
    void visitHardLinkArchiveFile(HardLinkArchiveFile hardLinkArchiveFile);
}
