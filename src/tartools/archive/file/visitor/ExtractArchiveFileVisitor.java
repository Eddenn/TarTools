package tartools.archive.file.visitor;

import tartools.archive.file.*;
import tartools.archive.options.AbstractOptionsSupport;
import tartools.archive.options.OptionsManager;
import tartools.util.BoundedInputStream;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class ExtractArchiveFileVisitor extends AbstractOptionsSupport implements ArchiveFileVisitor {

    private String dest;

    public ExtractArchiveFileVisitor() {
        super();
    }

    public ExtractArchiveFileVisitor(String dest, OptionsManager options) {
        super(options);
        this.dest = dest;
    }

    @Override
    public void visitDirectoryArchiveFile(DirectoryArchiveFile archiveFile) {
        testExtract(archiveFile);
    }

    @Override
    public void visitStandardArchiveFile(StandardArchiveFile archiveFile) {
        testExtract(archiveFile);
    }

    @Override
    public void visitSymbolicLinkArchiveFile(SymbolicLinkArchiveFile archiveFile) {
        testExtract(archiveFile);
    }

    @Override
    public void visitHardLinkArchiveFile(HardLinkArchiveFile archiveFile) {
        testExtract(archiveFile);
    }

    /**
     * Extrait le fichier d'archive si il vérifie les options
     * @param archiveFile le fichier d'archive
     */
    private void testExtract(ArchiveFile archiveFile) {
        //Si le fichier valide tous les filtres
        if(getOptionsManager().validateAllFilter(archiveFile)) {
            try {
                if (getOptionsManager().containsKey("v")) {
                    String fileDest = extract(dest, archiveFile);
                    if (fileDest != null) {
                        System.out.println(fileDest);
                    }
                } else {
                    extract(dest, archiveFile);
                }
            } catch (IOException e) {
                System.out.println("Erreur lors de l'écriture du fichier " + e.getMessage());
            }
        }
    }

    /**
     * Extrait le fichier d'archive (a) à la destination (dest)
     * @param dest répertoire de destination
     * @param a fichier d'archive
     * @throws IOException
     * @return le chemin absolu vers le fichier extrait (null si non extrait)
     */
    private String extract(String dest, ArchiveFile a) throws IOException {
        if(! (a instanceof DirectoryArchiveFile) ) {
            //On récupère le nom relatif à l'archive
            String fileName = a.getHeader().getFullName();
            //On crée le chemin total
            Path pathToFile = Paths.get(dest + "/" + fileName);
            //On crée les répertoires manquants
            Files.createDirectories(pathToFile.getParent());
            //On crée le fichier
            Files.createFile(pathToFile);
            //----- On écrit le fichier en fonction de son contenu -----
            File fileCreated = pathToFile.toFile();
            //On estime son Charset
            // (Arbitrairement UTF-8 car necessiterai une détection par une librairie externe ou autre algorithme lourd)
            Charset charset = StandardCharsets.UTF_8;
            //On crée le writer
            OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(fileCreated), charset);
            //On récupère le contenu du fichier
            BoundedInputStream inputStream = a.getContent();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, charset));
            //On écrit dans le fichier
            int cReaded;
            while ((cReaded = in.read()) != -1) {
                fileWriter.write(cReaded);
            }
            //On ferme les stream
            in.close();
            inputStream.close();
            fileWriter.close();
            //----------------------------------------------------------
            return pathToFile.toAbsolutePath().toString();
        } else {
            return null;
        }
    }
}
