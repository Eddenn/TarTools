package tartools.archive.file.visitor;

import tartools.archive.file.*;
import tartools.archive.file.header.ExtendedTarHeader;
import tartools.archive.options.AbstractOptionsSupport;
import tartools.archive.options.OptionsManager;

public class ListArchiveFileVisitor extends AbstractOptionsSupport implements ArchiveFileVisitor{

    public ListArchiveFileVisitor() {
        super();
    }

    public ListArchiveFileVisitor(OptionsManager options) {
        super(options);
    }

    @Override
    public void visitDirectoryArchiveFile(DirectoryArchiveFile archiveFile) {
        showDirectoryLine(archiveFile);
    }

    @Override
    public void visitStandardArchiveFile(StandardArchiveFile archiveFile) {
        showFileLine(archiveFile);
    }

    @Override
    public void visitSymbolicLinkArchiveFile(SymbolicLinkArchiveFile archiveFile) {
        showFileLine(archiveFile);
    }

    @Override
    public void visitHardLinkArchiveFile(HardLinkArchiveFile archiveFile) {
        showFileLine(archiveFile);
    }

    /**
     * Affiche la ligne représentant le fichier d'archive (prend en compte les options et les filtres)
     * @param archiveFile le fichier d'archive
     */
    private void showDirectoryLine(ArchiveFile archiveFile) {
        //Si le fichier valide tous les filtres
        if(getOptionsManager().validateAllFilter(archiveFile)) {
            //Si option "v" alors affichage détaillé
            if (getOptionsManager().containsKey("v")) {
                System.out.print(toDirectoryLine(archiveFile, true));
            } else {
                System.out.print(toDirectoryLine(archiveFile, false));
            }
        }
    }

    /**
     * Affiche la ligne correspondant au répetoire actuel
     * @param detailed si la ligne est plus détaillée
     * @return la ligne correspondant au répetoire actuel
     */
    private static String toDirectoryLine(ArchiveFile a, boolean detailed) {
        String archiveFileName = a.getHeader().getFullName();
        int level = archiveFileName.length() - archiveFileName.replace("/", "").length();
        if(a.getHeader().getTypeFlag() == 5) level--;
        //Tabulation
        String sRet = new String(new char[level]).replace("\0", "\t");
        //Nom du fichier
        sRet += a.toString();
        if(detailed) {
            //Information détaillée
            sRet += " (";
            if(a.getHeader().getMagic().equals("ustar")) {
                ExtendedTarHeader extHeader = (ExtendedTarHeader)a.getHeader();
                sRet += "uid="+extHeader.getUName()+"("+a.getHeader().getUid()+")";
                sRet += ",gid="+extHeader.getGName()+"("+a.getHeader().getGid()+")";
            } else {
                sRet += "uid="+a.getHeader().getUid();
                sRet += ",gid="+a.getHeader().getGid();
            }
            sRet += ",mode="+a.getHeader().getFormatedMode();
            sRet += ")";
        }
        return sRet+'\n';
    }

    /**
     * Affiche la ligne représentant le fichier d'archive (prend en compte les options et les filtres)
     * @param archiveFile le fichier d'archive
     */
    private void showFileLine(ArchiveFile archiveFile) {
        //Si le fichier valide tous les filtres
        if(getOptionsManager().validateAllFilter(archiveFile)) {
            //Si option "v" alors affichage détaillé
            if (getOptionsManager().containsKey("v")) {
                System.out.print(toFileLine(archiveFile, true));
            } else {
                System.out.print(toFileLine(archiveFile, false));
            }
        }
    }

    /**
     * Affiche la ligne correspondant au fichier actuel
     * @param detailed si la ligne est plus détaillée
     * @return la ligne correspondant au fichier actuel
     */
    private static String toFileLine(ArchiveFile a, boolean detailed) {
        String archiveFileName = a.getHeader().getFullName();
        int level = archiveFileName.length() - archiveFileName.replace("/", "").length();
        if(a.getHeader().getTypeFlag() == 5) level--;
        //Tabulation
        String sRet = new String(new char[level]).replace("\0", "\t");
        //Nom du fichier
        sRet += a.toString();
        if(detailed) {
            //Information détaillée
            sRet += " (";
            sRet += "size="+a.getHeader().getSize();
            sRet += ",date="+a.getHeader().getFormatedMTime();
            if(a.getHeader().getMagic().equals("ustar")) {
                ExtendedTarHeader extHeader = (ExtendedTarHeader)a.getHeader();
                sRet += ",uid="+extHeader.getUName()+"("+a.getHeader().getUid()+")";
                sRet += ",gid="+extHeader.getGName()+"("+a.getHeader().getGid()+")";
            } else {
                sRet += ",uid="+a.getHeader().getUid();
                sRet += ",gid="+a.getHeader().getGid();
            }
            sRet += ",mode="+a.getHeader().getFormatedMode();
            sRet += ")";
        }
        return sRet+'\n';
    }
}
