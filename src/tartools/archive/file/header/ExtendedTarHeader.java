package tartools.archive.file.header;

import java.util.Map;

/**
 * En-tête d'une archive tar sous la Ustar (norme POSIX 1003.1-1990)
 */
public class ExtendedTarHeader extends BasicTarHeader {

    /**
     * Constructeur
     * @param headerList liste des entête
     */
    public ExtendedTarHeader(Map<String, byte[]> headerList) {
        super(headerList);
    }

    @Override
    public String getFullName() {
        return parseString("name") +  parseString("prefix");
    }

    /**
     * Accesseur de la version (les caractères « 00 » indiquent un format POSIX 1003.1-1990. Deux espaces indique le format vieux GNU (à ne plus utiliser))
     * @return la version du fichier
     */
    public String getVersion() {
        return parseString("version");
    }

    /**
     * Accesseur du nom du propriétaire
     * @return le nom du propriétaire du fichier
     */
    public String getUName() {
        return parseString("uname");
    }

    /**
     * Accesseur du nom du groupe propriétaire
     * @return le nom du groupe propriétaire du fichier
     */
    public String getGName() {
        return parseString("gname");
    }

    /**
     * Accesseur du numéro majeur
     * @return le numéro majeur du fichier
     */
    public int getDevMajor() {
        return Integer.parseInt(parseString("devmajor"),8);
    }

    /**
     * Accesseur du numéro mineur
     * @return le numéro mineur du fichier
     */
    public int getDevMinor() {
        return Integer.parseInt(parseString("devminor"),8);
    }

    /**
     * Accesseur du préfix
     * @return le préfix du fichier
     */
    public String getPrefix() {
        return parseString("prefix");
    }
}
