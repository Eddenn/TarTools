package tartools.archive.file.header;

import java.util.Map;

/**
 * Interface d'un en-tête de fichier
 */
public interface ArchiveFileHeader {

    /**
     * Accesseur de la liste des en-tête
     * @return la liste des en-tête du fichier
     */
    Map<String,byte[]> getHeaderList();

    /**
     * Accesseur de de magic (ce champ indique s'il s'agit d'un en-tête étendu. Il vaut alors "ustar")
     * @return le champ magic du fichier
     */
    String getMagic();

    /**
     * Accesseur du nom
     * @return le nom du fichier
     */
    String getName();

    /**
     * Accesseur du nom entier (cas d'entête étendu)
     * @return le nom entier du fichier
     */
    String getFullName();

    /**
     * Accesseur du mode
     * @return le mode du fichier
     */
    String getMode();

    /**
     * Accesseur du mode formaté
     * @return le mode formaté du fichier
     */
    String getFormatedMode();

    /**
     * Accesseur de l'uid
     * @return l'uid du fichier
     */
    String getUid();

    /**
     * Accesseur du gid
     * @return le gid du fichier
     */
    String getGid();

    /**
     * Accesseur de la taille
     * @return la taille du fichier
     */
    int getSize();

    /**
     * Accesseur de la date de dernière modification
     * @return la date de dernière modification du fichier
     */
    long getMTime();

    /**
     * Accesseur de la somme de contrôle
     * @return la somme de contrôle du fichier
     */
    String getChksum();

    /**
     * Accesseur du type du fichier
     * @return le type du fichier
     */
    int getTypeFlag();

    /**
     * Accesseur du nom du fichier pointé par le lien symbolique
     * @return le fichier pointé par le lien symbolique
     */
    String getLinkName();

    /**
     * Accesseur de la date de dernière modification formatée
     * @return la date dernière modification formatée
     */
    String getFormatedMTime();
}
