package tartools.archive.file.header;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * En-tête basique d'une archive tar
 */
public class BasicTarHeader implements ArchiveFileHeader{
    private Map<String, byte[]> headerList;

    /**
     * Constructeur
     * @param headerList liste des en-tête
     */
    public BasicTarHeader(Map<String, byte[]> headerList) {
        this.headerList = headerList;
    }

    protected String parseString(String key) {
        return new String(headerList.get(key)).trim().replaceFirst("^0+(?!$)", "");
    }

    @Override
    public Map<String, byte[]> getHeaderList() {
        return headerList;
    }

    @Override
    public String getMagic() {
        return parseString("magic");
    }

    @Override
    public String getName() {
        return parseString("name");
    }

    @Override
    public String getFullName() {
        return parseString("name");
    }

    @Override
    public String getMode() {
        return parseString("mode");
    }

    @Override
    public String getFormatedMode() {
        char[] nums = getMode().toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char s : nums) {
            int num = Integer.parseInt(""+s);
            sb.append((num & 4) == 0 ? '-' : 'r');
            sb.append((num & 2) == 0 ? '-' : 'w');
            sb.append((num & 1) == 0 ? '-' : 'x');
        }
        return sb.toString();
    }

    @Override
    public String getUid() {
        return parseString("uid");
    }

    @Override
    public String getGid() {
        return parseString("gid");
    }

    @Override
    public int getSize() {
        return Integer.parseInt( parseString("size"), 8);
    }

    @Override
    public long getMTime() {
        return Long.parseLong( parseString("mtime"), 8);
    }

    @Override
    public String getFormatedMTime() {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date date = new Date(getMTime() * 1000);
        return dt.format(date);
    }

    @Override
    public String getChksum() {
        return parseString("chksum");
    }

    @Override
    public int getTypeFlag() {
        return Integer.parseInt( parseString("typeflag"), 8);
    }

    @Override
    public String getLinkName() {
        return parseString("linkname");
    }

    @Override
    public String toString() {
        String sRet = "";
        for(Map.Entry entry : headerList.entrySet()) {
            try {
                sRet += entry.getKey()+" -> "+new String((byte[])entry.getValue(),"US-ASCII")
                        +Arrays.toString((byte[])entry.getValue())
                        +"\n";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return sRet;
    }
}
