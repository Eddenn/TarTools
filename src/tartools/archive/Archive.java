package tartools.archive;

import tartools.archive.file.ArchiveFile;
import tartools.archive.visitor.ArchiveVisitor;

import java.util.List;

/**
 * Classe représentant une archive
 */
public interface Archive {

    /**
     * Méthode permettant l'exécution d'une commande
     * @param v le visiteur executant la commande
     */
    void accept(ArchiveVisitor v);

    /**
     * Accesseur de la liste des fichiers contenant les fichiers situés à la racine
     * @return liste des fichiers contenant les fichiers situés à la racine
     */
    List<ArchiveFile> getFiles();

    /**
     * Méthode permettant d'ajouter une entrée dans la liste des fichiers
     * @param file fichier
     */
    void addFile(ArchiveFile file);

    /**
     * Méthode permettant de supprimer un fichier de la liste des fichiers
     * @param i index dans la liste
     */
    void removeFile(int i);

    /**
     * Méthode permettant de supprimer un fichier de la liste des fichiers
     * @param fileName nom du fichier
     */
    void removeFile(String fileName);

    /**
     * Accesseur d'un fichier de la liste
     * @param i index dans la liste
     * @return fichier
     */
    ArchiveFile getFile(int i);

    /**
     * Accesseur d'un fichier de la liste
     * @param fileName nom du fichier
     * @return fichier
     */
    ArchiveFile getFile(String fileName);
}
